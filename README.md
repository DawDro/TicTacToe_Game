# TicTacToe Game

The project is version of simple TicTacToe game created in python based on classes to train python language and OOP programming.

The project was created as part of ZeroToJunior programming python coaching program - one of the exercises to get to next program level.

The game has simple GUI created in tkinter with buttons.