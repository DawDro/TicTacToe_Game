import tkinter as tk
from tkinter import messagebox
import random

class TicTacToe:
    def __init__(self, game_window, vs_computer=False):
        self.window = game_window
        self.window.title("Kółko - Krzyżyk")
        self.window.geometry("270x290")
        self.sign = "X"
        self.vs_computer = vs_computer
        self.create_game_area()

    def create_game_area(self):
        self.buttons = []
        for btn_num in range(9):
            row_num = btn_num // 3
            col_num = btn_num % 3
            button = tk.Button(master=self.window, text="", width=10, height=5, command=lambda b=btn_num: self.player_move(b))
            button.grid(row=row_num, column=col_num, padx=5, pady=5)
            self.buttons.append(button)

    def player_move(self, index):
        if self.buttons[index]["text"] == "":
            self.buttons[index]["text"] = self.sign
            if self.check_winner():
                messagebox.showinfo("Koniec gry", f"Gracz {self.sign} wygrał!")
                self.reset_game()
            elif all(button["text"] != "" for button in self.buttons):
                messagebox.showinfo("Koniec gry", "Remis!")
                self.reset_game()
            else:
                self.sign = "O" if self.sign == "X" else "X"
                if self.vs_computer and self.sign == "O":
                    self.computer_move()

    def computer_move(self):
        available_moves = [i for i, button in enumerate(self.buttons) if button["text"] == ""]
        if available_moves:
            move = random.choice(available_moves)
            self.buttons[move]["text"] = self.sign
            if self.check_winner():
                messagebox.showinfo("Koniec gry", f"Gracz {self.sign} wygrał!")
                self.reset_game()
            elif all(button["text"] != "" for button in self.buttons):
                messagebox.showinfo("Koniec gry", "Remis!")
                self.reset_game()
            else:
                self.sign = "X"

    def check_winner(self):
        # Row check
        for i in range(0, 9, 3):
            if self.buttons[i]["text"] == self.buttons[i+1]["text"] == self.buttons[i+2]["text"] and self.buttons[i]["text"] != "":
                return True

        # Column check
        for i in range(3):
            if self.buttons[i]["text"] == self.buttons[i+3]["text"] == self.buttons[i+6]["text"] and self.buttons[i]["text"] != "":
                return True

        # X check
        if self.buttons[0]["text"] == self.buttons[4]["text"] == self.buttons[8]["text"] and self.buttons[0]["text"] != "":
            return True
        if self.buttons[2]["text"] == self.buttons[4]["text"] == self.buttons[6]["text"] and self.buttons[2]["text"] != "":
            return True

        return False

    def reset_game(self):
        for button in self.buttons:
            button["text"] = ""
        self.sign = "X"

if __name__ == "__main__":
    game_window = tk.Tk()
    game = TicTacToe(game_window, vs_computer=True)
    game_window.mainloop()
